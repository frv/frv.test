Sources of https://luc.frama.io

Generated with [Hugo](https://gohugo.io).

Theme: [Identity theme](http://themes.gohugo.io/theme/hugo-identity-theme/), slightly modified for my needs.

Uses [jQuery](https://jquery.com), [Font Awesome](http://fontawesome.io), [Vertical timeline](https://codyhouse.co/gem/vertical-timeline/) and [a sticky notes effect](http://code.tutsplus.com/tutorials/create-a-sticky-note-effect-in-5-easy-steps-with-css3-and-html5--net-13934).

The content is published under the [CC-0](http://creativecommons.org/publicdomain/zero/1.0/) license (see the [LICENSE file](LICENSE)).

Penguins images are from [Boulet](http://bouletcorp.com) and are submitted to classic Copyright.
As drawings offered on my books at a meeting with the author, I've got a usage right, but usage by other users may need permission from him.
