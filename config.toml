baseurl = "https://frv.frama.io/frv.test/"
languageCode = "fr"
title = "frv²Didry"
theme = "materialize-cards"
[params]

    # Images settings
    # Icons, backgrounds and avatars folders are placed in theme's static/images folder.
    # You may place your images in corresponding folders and then set only filenames below to
    # change page's appearance

    favicon = "favicon.png?v=1"
    appletouchicon = "apple-touch-icon-precomposed.png?v=2"
    background = "raptor.png"
    avatar = "luc.png"

    # Personal settings

    name = "Luc Didry"
    position = "Administrateur Systèmes, Perliste fou, Debianeux convaincu, Libriste radical"

    # Extra stuff
    # NOTE: There is little different extra stuff in original Identity theme by HTML5 UP.
    # You can play with it downlading original theme right here:
    # html5up.net/identity/download
    #
    # Since this template is static, the contact form uses www.formspree.io as a
    # proxy. The form makes a POST request to their servers to send the actual
    # email. Visitors can send up to a 1000 emails each month for free.
    #
    # What you need to do for the setup?
    #
    # - set your email address under 'email' below
    # - enable form by setting 'enable' to 'true'
    # - upload the generated site to your server
    # - send a dummy email yourself to confirm your account
    # - click the confirm link in the email from www.formspree.io
    # - you're done. Happy mailing!

    email = "luc+contact@didry.org"

    show_work = true
    work_title = "Réalisations"
    more_text  = "Voir plus"

    show_resume = true
    resume_title = "Parcours"

    show_skills = true
    skills_title = "Compétences"

    [[params.work]]
        name = "Lufi"
        more = "Site d’hébergement chiffré (côté client) de fichiers"
        url  = "https://demo.lufi.io"

    [[params.work]]
        name = "Lutim"
        more = "Site d’hébergement chiffré (côté serveur) d’images"
        url  = "https://lut.im"

    [[params.work]]
        name = "Lstu"
        more = "Raccourcisseur d’URL"
        url  = "https://lstu.fr"

    [[params.work]]
        name = "FsPages"
        more = "Publication automatique de pages depuis un serveur Gitlab"
        url  = "https://frama.io"

    [[params.work]]
        name = "Erco"
        more = "ExaBGP Routes Controller"
        url  = "https://erco.xyz"

    [[params.work]]
        name = "Diverses contributions à des projets libres"
        more = "Augeas, Owncloud, Etherpad, Ethercalc, Liquidprompt, Searx, Gitlab, Apticron, Weboob, Turtl, Scrumblr…"

    [[params.work]]
        name = "WebHooker"
        more = "Web service permettant de mirrorer des dépôts Gitlab sur Github"
        url  = "https://framagit.org/luc/webhooker"

    [[params.work]]
        name = "GiphyMatHooker"
        more = "Web service permettant de rechercher des gifs sur Giphy depuis un serveur Mattermost"
        url  = "https://framagit.org/framasoft/giphymathooker"

    [[params.work]]
        name = "Dynamic Motd"
        more = "Un motd dynamique pour Debian et dérivées"
        url  = "https://framagit.org/luc/dynamic-motd"

    [[params.work]]
        name = "ERT"
        more = "Service web ajoutant un historique à un serveur Ethercalc"
        url  = "https://framagit.org/framasoft/ert"

    [[params.work]]
        name = "Divers modules Perl"
        more = ""
        url  = "https://metacpan.org/author/LDIDRY"

    [[params.work]]
        name = "ep countable"
        more = "Plugin Etherpad affichant des statistiques sur le pad"
        url  = "https://framagit.org/luc/ep_countable"

    [[params.work]]
        name = "ep delete_empty_pads"
        more = "Plugin Etherpad supprimant automatiquement les pads vides"
        url  = "https://framagit.org/luc/ep_delete_empty_pads"

    [[params.work]]
        name = "ep delete_after_delay"
        more = "Plugin Etherpad permettant d'avoir des pads temporaires"
        url  = "https://framagit.org/luc/ep_delete_after_delay"

    [[params.work]]
        name = "ep pads_stats"
        more = "Plugin Etherpad permettant d'avoir des statistiques sur le serveur Etherpad"
        url  = "https://framagit.org/luc/ep_pads_stats"

    [[params.work]]
        name = "Etherpad::Admin"
        more = "Interface web permettant de gérer les pads d'un serveur etherpad"
        url  = "https://framagit.org/luc/etherpad-admin"

    [[params.work]]
        name = "Diverses sondes Munin"
        more = ""
        url  = "https://github.com/ldidry/munin-plugins"

    [[params.work]]
        name = "Munin coffee"
        more = "Sonde Munin pour monitorer le niveau de café dans une cafetière"
        url  = "https://framagit.org/luc/munin-coffee"

    [[params.skills]]
        name = "Systèmes type Unix tels que GNU/Linux (utilisation, programmation shell et programmation système) ;"

    [[params.skills]]
        name = "Réseaux locaux, protocoles de l’Internet ;"

    [[params.skills]]
        name = "Perl, Bash, Javascript, PHP, Python, Java, notions de Ruby ;"

    [[params.skills]]
        name = "Apache, Nginx, Iptables, Vuurmuur, Bind, Postfix, Shinken, Munin, Mysql, PostgreSQL, OpenLDAP, LXC, Ganeti, Vim, Git, Mojolicious…"

    [[params.resume]]
        from = "Janv. 2016"
        to   = "actuellement"
        what = "Administrateur systèmes au sein de l'association Framasoft, Nancy"
        icon = "work"

    [[params.resume]]
        from = "Mars 2011"
        to   = "Déc. 2015"
        what = "Administrateur systèmes et réseaux à l'université de Lorraine, Nancy"
        icon = "work"

    [[params.resume]]
        from = "Nov. 2010"
        to   = "Fév 2011"
        what = "Développeur PHP chez ABL SA (Advanced Biological Laboratories), Luxembourg "
        icon = "work"

    [[params.resume]]
        from = "Juil. 2010"
        to   = "Nov. 2010"
        what = "Administrateur systèmes chez Oxalide, société de conseil, infogérance et hébergement, Paris"
        icon = "work"

    [[params.resume]]
        from = "2009"
        to   = "2010"
        what = "Licence professionnelle ASRALL (Administration de Systèmes, Réseaux et Applications à base de Logiciels Libres) à l’IUT Charlemagne de Nancy Mention bien"
        icon = "graduation"

    [[params.resume]]
        from = "2008"
        to   = "2009"
        what = "Major du DUT Informatique à l’IUT Charlemagne de Nancy"
        icon = "graduation"

    # Social settings
    # You may change these with your social links or add others

    [[params.social]]
        title = "Diaspora"
        icon = "fa-asterisk"
        link = "https://framasphere.org/people/b13eb6b0beac0131e7e32a0000053625"
    [[params.social]]
        title = "Cours en ligne"
        icon = "fa-graduation-cap"
        link = "/cours-asrall"

    # Footer settings
    [params.footer]
        copyright = "Luc Didry"
